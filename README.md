User by default:
u: admin p: Admin1234 q: What is name of your pet? a: Murzik

/////////////////////////////////////////////////////////////////////////////////////

Git commands:

For clone repository:
git clone https://gitlab.com/lilagerasimova/sneakers-store.git

List of all commits:
git log

For show current repository status:
git status

For add file to commit candidate:
git add name_of_file

For add all files to commit candidate:
git add .

For create commit:
git commit -m 'my first commit'

For create new branch:
git checkout -b name_of_branch

For checkout to existing branch:
git checkout name_of_branch

For merge branch:
git merge name_of_branch

For push to remote repository:
git push