package ru.hse.sneakersstore.store;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public abstract class StoreModel {

    @Expose
    @SerializedName("id")
    protected long mId;

    public StoreModel(long id) {
        this.mId = id;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        this.mId = id;
    }
}
