package ru.hse.sneakersstore.store;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedHashTreeMap;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.util.*;

public class StoreReader {

    // Input file
    private File mInput;

    // Readed models
    private Map<Long, Object> mModels;

    // Serialization library
    private Gson mGson;

    public StoreReader(File input) {
        mInput = input;
        mModels = new HashMap<>();
        mGson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }

    public <V extends StoreModel> V get(Long key, Class<V> clazz) throws IOException {
        load(clazz);
        return (V) mModels.get(key);
    }

    public <V extends StoreModel> List<V> getList(Class<V> clazz) throws IOException {
        load(clazz);

        List<V> list = new ArrayList<V>();

        for (Object obj : mModels.values()) {
            list.add((V) obj);
        }

        return list;
    }

    public <V extends StoreModel> Map<Long, V> getMap(Class<V> clazz) throws IOException {
        load(clazz);

        Map<Long, V> map = new HashMap<Long, V>();

        for (Map.Entry<Long, Object> entry : mModels.entrySet()) {
            map.put(entry.getKey(), (V) entry.getValue());
        }

        return map;
    }

    public Set<Long> keySet() {
        return mModels.keySet();
    }

    private <V extends StoreModel> void load(Class<V> clazz) throws IOException {
        String file = readFromFile();

        Type type = new TypeToken<Map<Long, LinkedHashTreeMap<String, String>>>() {}.getType();
        Map<Long, LinkedHashTreeMap<String, String>> objects = mGson.fromJson(file, type);

        if (objects == null)
            return;

        for (Map.Entry<Long, LinkedHashTreeMap<String, String>> entry : objects.entrySet()) {
            JsonObject obj = mGson.toJsonTree(entry.getValue()).getAsJsonObject();
            mModels.put(entry.getKey(), mGson.fromJson(obj, clazz));
        }
    }

    private String readFromFile() throws IOException {
        byte[] bytes = Files.readAllBytes(mInput.toPath());
        return new String(bytes);
    }
}
