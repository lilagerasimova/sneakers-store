package ru.hse.sneakersstore.store;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

public class StoreWriter {

    // Output file
    private File mOutput;

    // Writable models
    private Map<Long, Object> mModels;

    // Serialization library
    private Gson mGson;

    public StoreWriter(File output) {
        mOutput = output;
        mModels = new HashMap<>();
        mGson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();
    }

    public <V extends StoreModel> void put(V value) {
        mModels.put(value.getId(), value);
    }

    public <V extends StoreModel> void putAll(Map<Long, V> objects) {
        mModels.putAll(objects);
    }

    public void close() throws IOException {
        writeToFile();
    }

    private void writeToFile() throws IOException {
        String json = mGson.toJson(mModels);
        
        Files.deleteIfExists(mOutput.toPath());
        Files.write(mOutput.toPath(), json.getBytes(), StandardOpenOption.CREATE);
    }
}
