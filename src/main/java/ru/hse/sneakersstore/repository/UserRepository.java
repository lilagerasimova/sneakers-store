package ru.hse.sneakersstore.repository;

import ru.hse.sneakersstore.common.Const;
import ru.hse.sneakersstore.models.User;
import ru.hse.sneakersstore.store.StoreReader;
import ru.hse.sneakersstore.store.StoreWriter;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class UserRepository {

    private StoreWriter mWriter;
    private StoreReader mReader;

    public UserRepository() {
        File file = new File(Const.FILES.USERS);
        mWriter = new StoreWriter(file);
        mReader = new StoreReader(file);
    }

    public void add(User user) {
        try {
            Map<Long, User> users = mReader.getMap(User.class);

            long id = generateId();

            user.setId(id);
            user.setDeleted(false);

            users.put(id, user);

            mWriter.putAll(users);
            mWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public User getByLogin(String login) {
    	try {
            Map<Long, User> users = mReader.getMap(User.class);            
            for (User user : users.values()) {
                if (login.equals(user.getLogin()))
                    return user;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    	
    	return null;
    }
    
    public List<User> getAll() {
        try {
            return mReader.getList(User.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
    
    public void update(User user) {
    	try {
			Map<Long, User> users = mReader.getMap(User.class);
			
			users.put(user.getId(), user);
			
			mWriter.putAll(users);
			mWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    } 

    private long generateId() {
        long max = 0;
        for (Long id: mReader.keySet()) {
            if (id > max)
                max = id;
        }
        return ++max;
    }
}
