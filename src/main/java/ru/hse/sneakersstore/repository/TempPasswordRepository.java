package ru.hse.sneakersstore.repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import ru.hse.sneakersstore.common.Const;
import ru.hse.sneakersstore.models.TempPassword;
import ru.hse.sneakersstore.store.StoreReader;
import ru.hse.sneakersstore.store.StoreWriter;

public class TempPasswordRepository {
	
    private StoreWriter mWriter;
    private StoreReader mReader;

    public TempPasswordRepository() {
        File file = new File(Const.FILES.TEMP_PASSWORDS);
        mWriter = new StoreWriter(file);
        mReader = new StoreReader(file);
    }

    public void add(TempPassword password) {
    	try {
			Map<Long, TempPassword> passwords = mReader.getMap(TempPassword.class);
			
			long id = generateId();
			
			password.setId(id);
			
			passwords.put(id, password);
			
			mWriter.putAll(passwords);
			mWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public List<TempPassword> getAllByUserId(long id) {
    	try {
    		List<TempPassword> passwords = mReader.getList(TempPassword.class);      
    		
    		List<TempPassword> filteredPasswords = new ArrayList<>();
    		      
            for (TempPassword item : passwords) {
                if (id == item.getUserId())
                    filteredPasswords.add(item);
            }
            
            return filteredPasswords;
        } catch (IOException e) {
            e.printStackTrace();
        }
    	
    	return Collections.emptyList();
    }
    
    private long generateId() {
        long max = 0;
        for (Long id: mReader.keySet()) {
            if (id > max)
                max = id;
        }
        return ++max;
    }
}
