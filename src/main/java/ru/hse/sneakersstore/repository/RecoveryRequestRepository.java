package ru.hse.sneakersstore.repository;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import ru.hse.sneakersstore.common.Const;
import ru.hse.sneakersstore.models.RecoveryRequest;
import ru.hse.sneakersstore.models.User;
import ru.hse.sneakersstore.store.StoreReader;
import ru.hse.sneakersstore.store.StoreWriter;

public class RecoveryRequestRepository {

    private StoreWriter mWriter;
    private StoreReader mReader;

    public RecoveryRequestRepository() {
        File file = new File(Const.FILES.RECOVERY_REUESTS);
        mWriter = new StoreWriter(file);
        mReader = new StoreReader(file);
    }
	
    public void add(RecoveryRequest request) {
    	try {
			Map<Long, RecoveryRequest> requests = mReader.getMap(RecoveryRequest.class);
			
			long id = generateId();
			
			request.setId(id);
		
			requests.put(id, request);
			
			mWriter.putAll(requests);
			mWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public List<RecoveryRequest> getAll() {
        try {
            return mReader.getList(RecoveryRequest.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
    
    public void update(RecoveryRequest request) {
    	try {
			Map<Long, RecoveryRequest> requests = mReader.getMap(RecoveryRequest.class);
			
			requests.put(request.getId(), request);
			
			mWriter.putAll(requests);
			mWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    private long generateId() {
        long max = 0;
        for (Long id: mReader.keySet()) {
            if (id > max)
                max = id;
        }
        return ++max;
    }
}
