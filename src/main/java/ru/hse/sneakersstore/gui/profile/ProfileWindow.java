package ru.hse.sneakersstore.gui.profile;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import ru.hse.sneakersstore.Main;
import ru.hse.sneakersstore.models.User;
import ru.hse.sneakersstore.repository.UserRepository;

public class ProfileWindow {

	private static final String EMAIL_PATTERN = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))(?=\\S+$)?$";

	protected Shell mShell;

	private Text mTextFirstName;
	private Text mTextLastName;
	private Text mTextEmail;

	private DateTime mDateBirthday;

	private Combo mComboAddressForm;
	private Combo mComboBlockReason;

	private Button mButtonVerify;
	private Button mButtonAssignAsAnalyst;
	private Button mButtonBlockUser;

	private TabFolder mTabFolder;

	private TabItem mTabGeneral;
	private TabItem mTabUsers;

	private Table mTableUsers;

	private UserRepository mUserRepository;

	private User mUser;

	private List<User> mUsers;

	public ProfileWindow() {
		mUserRepository = new UserRepository();
		mUser = Main.sAppUser;
		mUsers = new ArrayList<>();
	}

	/**
	 * Open the window.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		mShell.open();
		mShell.layout();

		init();

		while (!mShell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private void init() {
		mTextFirstName.setText(mUser.getFirstName());
		mTextLastName.setText(mUser.getLastName());
		mTextEmail.setText(mUser.getEmail());

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(mUser.getBirthday());

		mDateBirthday.setDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) - 1,
				calendar.get(Calendar.DAY_OF_MONTH));

		for (int i = 0; i < mComboAddressForm.getItems().length; i++) {
			String item = mComboAddressForm.getItems()[i];
			if (item.equals(mUser.getAddressForm())) {
				mComboAddressForm.select(i);
				break;
			}
		}

		// Remove Users tab if current user is not admin
		if (!mUser.isAdmin()) {
			mTabUsers.dispose();
			return;
		}

		showUsers();
	}

	private void showUsers() {
		mUsers.clear();
		mTableUsers.removeAll();

		List<User> users = mUserRepository.getAll();

		for (User user : users) {
			// Filter out deleted users
			if (user.isDeleted())
				continue;

			// Add to list users
			mUsers.add(user);

			// Add table item
			TableItem item = new TableItem(mTableUsers, SWT.NONE);
			item.setText(new String[] { user.getLogin(), user.getFirstName(), user.getLastName(), user.getEmail(),
					String.valueOf(user.isAnalyst()), String.valueOf(user.isVerified()) });
		}
	}

	private void saveProfile() {
		// Validate first name
		if (mTextFirstName.getText().length() < 1) {
			showError("First Name must be not empty!");
			return;
		}

		// Validate last name
		if (mTextLastName.getText().length() < 1) {
			showError("Last Name must be not empty!");
			return;
		}

		// Validate email
		if (!mTextEmail.getText().matches(EMAIL_PATTERN)) {
			showError("Email is not correct!");
			return;
		}

		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, mDateBirthday.getDay());
		calendar.set(Calendar.MONTH, mDateBirthday.getMonth());
		calendar.set(Calendar.YEAR, mDateBirthday.getYear());

		String firstName = mTextFirstName.getText();
		String lastName = mTextLastName.getText();
		String addressForm = mComboAddressForm.getText();
		Date birthday = calendar.getTime();

		String email = mTextEmail.getText();

		mUser.setFirstName(firstName);
		mUser.setLastName(lastName);
		mUser.setAddressForm(addressForm);
		mUser.setBirthday(birthday);
		mUser.setEmail(email);

		mUserRepository.update(mUser);
		showMessage("User was updated successfully");
	}

	private void deleteProfile() {
		MessageBox messageBox = new MessageBox(mShell, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
		messageBox.setMessage("Do you really want to delete your profile?");
		messageBox.setText("Confirm delete profile");

		int response = messageBox.open();

		if (response == SWT.YES) {
			// Mark user as deleted
			mUser.setDeleted(true);
			// Update user
			mUserRepository.update(mUser);
			// Clear current app user
			Main.sAppUser = null;
			// Show message
			showMessage("Profile was deleted successfully");
			// Close the app
			System.exit(0);
		}
	}

	private void verifySelectedUser(int position) {
		if (position == -1) {
			showMessage("Please select user");
			return;
		}
		
		User user = mUsers.get(position);

		// Mark user as verified
		user.setVerified(true);
		// Verify selected user
		mUserRepository.update(user);
		// Show message
		showMessage("User is verified");
		// Update list
		showUsers();
	}

	private void assignAsAnalystSelectedUser(int position) {
		if (position == -1) {
			showMessage("Please select user");
			return;
		}
		
		User user = mUsers.get(position);

		// Mark user
		user.setAnalyst(!user.isAnalyst());
		// Assign as analyst selected user
		mUserRepository.update(user);
		// Show message
		if (!user.isAnalyst()) {
			showMessage("Changed. User is anayst");
		} else {
			showMessage("Changed. User is not anayst");
		}
		// Update list
		showUsers();
	}
	
	private void blockSelectedUser(int position) {
		if (position == -1) {
			showMessage("Please select user");
			return;
		}
		
		User user = mUsers.get(position);
		
		// Selected reason for block
		String reason = mComboBlockReason.getItems()[mComboBlockReason.getSelectionIndex()];
		// Mark user
		user.setBlocked(!user.isBlocked());
		// Assign as analyst selected user
		mUserRepository.update(user);
		// Show message
		if (!user.isBlocked()) {
			showMessage("Changed. User is blocked");
		} else {
			showMessage("Changed. User is unblocked");
		}
		// Update list
		showUsers();
	}

	private void handleSelectedUser(int position) {
		User user = mUsers.get(position);

		if (user.isAnalyst()) {
			mButtonAssignAsAnalyst.setText("Revoke analyst");
		} else {
			mButtonAssignAsAnalyst.setText("Assign as analyst");
		}
		
		if (user.isBlocked()) {
			mButtonBlockUser.setText("Unblock user");
		} else {
			mButtonBlockUser.setText("Block user");
		}
		
		if (user.getId() == mUser.getId()) {
			mButtonBlockUser.setEnabled(false);
		} else {
			mButtonBlockUser.setEnabled(true);
		}
	}

	private void runRecoveryRequestsWindow() {
		RecoveryRequestsWindow window = new RecoveryRequestsWindow();
		window.open();
	}
	
	private void showMessage(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}

	private void showError(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		mShell = new Shell();
		mShell.setSize(638, 478);
		mShell.setText("Profile");

		TabFolder tabFolder = new TabFolder(mShell, SWT.NONE);
		tabFolder.setBounds(10, 10, 602, 419);

		mTabGeneral = new TabItem(tabFolder, SWT.NONE);
		mTabGeneral.setText("General");

		Composite composite = new Composite(tabFolder, SWT.NONE);
		mTabGeneral.setControl(composite);

		Label labelFirstName = new Label(composite, SWT.NONE);
		labelFirstName.setBounds(10, 10, 76, 15);
		labelFirstName.setText("First Name");

		mTextFirstName = new Text(composite, SWT.BORDER);
		mTextFirstName.setBounds(10, 37, 126, 21);

		Label labelLastName = new Label(composite, SWT.NONE);
		labelLastName.setBounds(10, 64, 76, 15);
		labelLastName.setText("Last Name");

		mTextLastName = new Text(composite, SWT.BORDER);
		mTextLastName.setBounds(10, 85, 126, 21);

		Label labelAddressForm = new Label(composite, SWT.NONE);
		labelAddressForm.setBounds(10, 112, 76, 15);
		labelAddressForm.setText("Adderss form");

		mComboAddressForm = new Combo(composite, SWT.NONE);
		mComboAddressForm.setItems(new String[] { "Mr.", "Mrs." });
		mComboAddressForm.setBounds(10, 132, 126, 23);
		mComboAddressForm.select(0);

		Label labelBirthday = new Label(composite, SWT.NONE);
		labelBirthday.setBounds(10, 162, 55, 15);
		labelBirthday.setText("Birthday");

		mDateBirthday = new DateTime(composite, SWT.BORDER);
		mDateBirthday.setBounds(10, 183, 126, 24);

		Label labelEmail = new Label(composite, SWT.NONE);
		labelEmail.setBounds(10, 213, 55, 15);
		labelEmail.setText("Email");

		mTextEmail = new Text(composite, SWT.BORDER);
		mTextEmail.setBounds(10, 238, 126, 21);

		Button buttonSave = new Button(composite, SWT.NONE);
		buttonSave.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				saveProfile();
			}
		});
		buttonSave.setBounds(482, 10, 89, 25);
		buttonSave.setText("Save");

		Button buttonDeleteProfile = new Button(composite, SWT.NONE);
		buttonDeleteProfile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				deleteProfile();
			}
		});
		buttonDeleteProfile.setBounds(482, 54, 89, 25);
		buttonDeleteProfile.setText("Delete Profile");

		mTabUsers = new TabItem(tabFolder, SWT.NONE);
		mTabUsers.setText("Users");

		Composite composite_1 = new Composite(tabFolder, SWT.NONE);
		mTabUsers.setControl(composite_1);

		mTableUsers = new Table(composite_1, SWT.BORDER | SWT.FULL_SELECTION);
		mTableUsers.setBounds(10, 76, 574, 255);
		mTableUsers.setHeaderVisible(true);
		mTableUsers.setLinesVisible(true);

		mTableUsers.addListener(SWT.Selection, new Listener() {

			@Override
			public void handleEvent(Event event) {
				handleSelectedUser(mTableUsers.getSelectionIndex());
			}
		});

		TableColumn columnLogin = new TableColumn(mTableUsers, SWT.NONE);
		columnLogin.setWidth(100);
		columnLogin.setText("Login");

		TableColumn columnFirstName = new TableColumn(mTableUsers, SWT.NONE);
		columnFirstName.setWidth(100);
		columnFirstName.setText("First Name");

		TableColumn columnLastName = new TableColumn(mTableUsers, SWT.NONE);
		columnLastName.setWidth(100);
		columnLastName.setText("Last Name");

		TableColumn columnEmail = new TableColumn(mTableUsers, SWT.NONE);
		columnEmail.setWidth(100);
		columnEmail.setText("Email");

		TableColumn columnAnalyst = new TableColumn(mTableUsers, SWT.NONE);
		columnAnalyst.setWidth(100);
		columnAnalyst.setText("Analyst");

		TableColumn columnVerified = new TableColumn(mTableUsers, SWT.NONE);
		columnVerified.setWidth(100);
		columnVerified.setText("Verified");

		mButtonVerify = new Button(composite_1, SWT.NONE);
		mButtonVerify.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				verifySelectedUser(mTableUsers.getSelectionIndex());
			}
		});
		mButtonVerify.setBounds(261, 356, 93, 25);
		mButtonVerify.setText("Verify User");

		mButtonAssignAsAnalyst = new Button(composite_1, SWT.NONE);
		mButtonAssignAsAnalyst.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				assignAsAnalystSelectedUser(mTableUsers.getSelectionIndex());
			}
		});
		mButtonAssignAsAnalyst.setBounds(360, 356, 101, 25);
		mButtonAssignAsAnalyst.setText("Assign as analyst");
		
		Button buttonShowRecoveryRequests = new Button(composite_1, SWT.NONE);
		buttonShowRecoveryRequests.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				runRecoveryRequestsWindow();
			}
		});
		buttonShowRecoveryRequests.setBounds(10, 26, 146, 25);
		buttonShowRecoveryRequests.setText("Show recovery requests");
		
		Label labelTools = new Label(composite_1, SWT.NONE);
		labelTools.setBounds(10, 5, 55, 15);
		labelTools.setText("Tools:");
		
		Label labelUsers = new Label(composite_1, SWT.NONE);
		labelUsers.setBounds(10, 57, 55, 15);
		labelUsers.setText("Users:");
		
		mComboBlockReason = new Combo(composite_1, SWT.NONE);
		mComboBlockReason.setItems(new String[] {"Spam", "Hack the system"});
		mComboBlockReason.setBounds(10, 358, 146, 23);
		mComboBlockReason.select(0);
		
		mButtonBlockUser = new Button(composite_1, SWT.NONE);
		mButtonBlockUser.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				blockSelectedUser(mTableUsers.getSelectionIndex());
			}
		});
		mButtonBlockUser.setBounds(162, 356, 93, 25);
		mButtonBlockUser.setText("Block user");
		
		Label lableReasonForBlock = new Label(composite_1, SWT.NONE);
		lableReasonForBlock.setBounds(10, 337, 124, 15);
		lableReasonForBlock.setText("Select reason for block");
	}
}
