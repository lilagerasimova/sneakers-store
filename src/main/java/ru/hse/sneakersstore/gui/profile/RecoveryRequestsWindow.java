package ru.hse.sneakersstore.gui.profile;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ru.hse.sneakersstore.models.RecoveryRequest;
import ru.hse.sneakersstore.models.TempPassword;
import ru.hse.sneakersstore.repository.RecoveryRequestRepository;
import ru.hse.sneakersstore.repository.TempPasswordRepository;

public class RecoveryRequestsWindow {

	protected Shell mShell;

	private Table mTableRequests;

	private RecoveryRequestRepository mRequestRepository;

	private TempPasswordRepository mPasswordRepository;

	private List<RecoveryRequest> mRequests;

	public RecoveryRequestsWindow() {
		mRequestRepository = new RecoveryRequestRepository();
		mPasswordRepository = new TempPasswordRepository();
		mRequests = new ArrayList<>();
	}

	/**
	 * Open the window.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		mShell.open();
		mShell.layout();

		showRequests();

		while (!mShell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private void showRequests() {
		mRequests.clear();
		mTableRequests.removeAll();

		List<RecoveryRequest> requests = mRequestRepository.getAll();

		for (RecoveryRequest request : requests) {
			// Filter out resolved
			if (request.isResolved())
				continue;

			// Add to list requests
			mRequests.add(request);

			// Add table item
			TableItem item = new TableItem(mTableRequests, SWT.NONE);
			item.setText(new String[] { request.getLogin(), request.getText() });
		}
	}

	private void resolveRequest(int position) {
		if (position == -1) {
			showMessage("Please select user");
			return;
		}

		RecoveryRequest request = mRequests.get(position);

		// Mark request as resolved
		request.setResolved(true);
		
		// Update request
		mRequestRepository.update(request);
					
		// Create temp password
		TempPassword password = new TempPassword(request.getUserId(), generateTempPassword());
		
		// Add password
		mPasswordRepository.add(password);
		
		// Show message
		showMessage("Changed. Request is resolved");
		
		// Update list
		showRequests();
	}

	private void showMessage(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}

	private void showError(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}

	private String generateTempPassword() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		mShell = new Shell();
		mShell.setSize(450, 329);
		mShell.setText("Recovery requests");

		mTableRequests = new Table(mShell, SWT.BORDER | SWT.FULL_SELECTION);
		mTableRequests.setBounds(10, 10, 414, 241);
		mTableRequests.setHeaderVisible(true);
		mTableRequests.setLinesVisible(true);

		TableColumn columnLogin = new TableColumn(mTableRequests, SWT.NONE);
		columnLogin.setWidth(99);
		columnLogin.setText("Login");

		TableColumn columnData = new TableColumn(mTableRequests, SWT.NONE);
		columnData.setWidth(314);
		columnData.setText("Request data");

		Button buttonResolve = new Button(mShell, SWT.NONE);
		buttonResolve.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				resolveRequest(mTableRequests.getSelectionIndex());
			}
		});
		buttonResolve.setBounds(10, 257, 139, 25);
		buttonResolve.setText("Resolve request");
	}
}
