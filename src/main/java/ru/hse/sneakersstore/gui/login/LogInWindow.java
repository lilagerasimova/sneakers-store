package ru.hse.sneakersstore.gui.login;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ru.hse.sneakersstore.Main;
import ru.hse.sneakersstore.gui.main.MainWindow;
import ru.hse.sneakersstore.gui.recovery.RecoveryWindow;
import ru.hse.sneakersstore.gui.registration.RegistrationWindow;
import ru.hse.sneakersstore.models.User;
import ru.hse.sneakersstore.repository.UserRepository;

public class LogInWindow {

	private static final int NUMBER_OF_ATTEMPTS = 3;
	
	protected Shell mShlLogin;
	
	private Text mTextLogin;
	private Text mTextPassword;
	
	private UserRepository mUserRepository;
	
	private Map<String, Integer> mAttemptsToLogIn;
	
	public LogInWindow() {
		mUserRepository = new UserRepository();
		mAttemptsToLogIn = new HashMap<>();
	}

	/**
	 * Open the window.
	 * @wbp.parser.entryPoint
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		mShlLogin.open();
		mShlLogin.layout();
		while (!mShlLogin.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}
	
	private void logIn() {
		// Validate login
		if (mTextLogin.getText().length() < 1) {
			showMessage("Login must not be empty");
			return;
		}
		
		// Validate password
		if (mTextPassword.getText().length() < 1) {
			showMessage("Password must not be empty");
			return;
		}
		
		String login = mTextLogin.getText();
		String password = mTextPassword.getText();
		
		// Check count of attempt to log in
		if (mAttemptsToLogIn.get(login) != null && mAttemptsToLogIn.get(login) > NUMBER_OF_ATTEMPTS - 2) {
			MessageBox messageBox = new MessageBox(mShlLogin, SWT.ICON_QUESTION | SWT.YES | SWT.NO);
			messageBox.setMessage("The number of attempts to log in was over. Do you want to reset password?");
			messageBox.setText("Attempts to log in");
			
			int response = messageBox.open();
			
			if (response == SWT.YES) {
				runRecoveryWindow();
			}
			
			return;
		}
		
		User user = mUserRepository.getByLogin(login);
		
		if (user == null) {
			showError(String.format("User with %s login is not found", login));
			return;
		}
		
		if (user.isDeleted()) {
			showError(String.format("User %s was deleted", login));
			return;
		}
           
        if (user.isAuthBlocked()) {
        	showError(String.format("User %s is blocked for authorization, try to send request to admin for resolve this problem", login));
			return;
        }
        	
        if (user.isBlocked()) {
        	showError(String.format("User %s was blocked", login));
        	return;
        }
            
        if (!user.isVerified()) {
        	showError(String.format("User %s is not verified", login));
        	return;
        }
        
        if (!user.getPassword().equals(password)) {
        	showError("Login and password don`t match");
        	
        	if (mAttemptsToLogIn.get(login) != null) {
        		int count = mAttemptsToLogIn.get(login);
        		mAttemptsToLogIn.put(login, ++count);
        	} else {
        		mAttemptsToLogIn.put(login, 0);
        	}
        	
        	return;
        }
		
		// Save user as app user
		Main.sAppUser = user;
		
		// Go to main window
		runMainWindow();
		
		// Close the window
		mShlLogin.dispose();
	}
	
	private void runMainWindow() {
		MainWindow window = new MainWindow();
		window.open();
	}
	
	private void runRegistrationWindow() {
		RegistrationWindow window = new RegistrationWindow();
		window.open();
	}
	
	private void runRecoveryWindow() {
		RecoveryWindow window = new RecoveryWindow();
		window.open();
	}
	
	private void showMessage(String message) {
		MessageBox mb = new MessageBox(mShlLogin);
		mb.setMessage(message);
		mb.open();
	}
	
	private void showError(String message) {
		MessageBox mb = new MessageBox(mShlLogin);
		mb.setMessage(message);
		mb.open();
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		mShlLogin = new Shell();
		mShlLogin.setText("Login");
		mShlLogin.setSize(200, 240);
		
		mTextLogin = new Text(mShlLogin, SWT.BORDER);
		mTextLogin.setBounds(10, 31, 154, 21);
		
		Label labelLogin = new Label(mShlLogin, SWT.NONE);
		labelLogin.setBounds(10, 10, 55, 15);
		labelLogin.setText("Login:");
		
		Label labelPassword = new Label(mShlLogin, SWT.NONE);
		labelPassword.setBounds(10, 58, 55, 15);
		labelPassword.setText("Password:");
		
		mTextPassword = new Text(mShlLogin, SWT.BORDER);
		mTextPassword.setBounds(10, 79, 154, 21);
		mTextPassword.setEchoChar('*');
		
		Button buttonLogin = new Button(mShlLogin, SWT.NONE);
		buttonLogin.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				logIn();
			}
		});
		buttonLogin.setBounds(10, 117, 154, 25);
		buttonLogin.setText("Log In");
		
		Link linkRegister = new Link(mShlLogin, SWT.NONE);
		linkRegister.setBounds(10, 169, 154, 15);
		linkRegister.setText("<a>Register new user</a>");
		linkRegister.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				runRegistrationWindow();
			}
		});
				
		Link linkResetPassword = new Link(mShlLogin, SWT.NONE);
		linkResetPassword.setBounds(10, 148, 147, 15);
		linkResetPassword.setText("<a>Forgot password</a>");
		linkResetPassword.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				runRecoveryWindow();
			}
		});
	}
}
