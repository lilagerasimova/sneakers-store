package ru.hse.sneakersstore.gui.registration;

import java.util.Calendar;
import java.util.Date;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ru.hse.sneakersstore.models.User;
import ru.hse.sneakersstore.repository.UserRepository;

public class RegistrationWindow {

	private static final String EMAIL_PATTERN = "^([_a-zA-Z0-9-]+(\\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*(\\.[a-zA-Z]{1,6}))(?=\\S+$)?$";
	private static final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
	private static final String LOGIN_PATTERN = "^(?=\\S+$).+$";
	
	protected Shell mShlRegistration;
	
	private Text mTextFirstName;
	private Text mTextLastName;
	private Text mTextEmail;
	private Text mTextLogin;
	private Text mTextPassword;
	private Text mTextPasswordAgain;
	private Text mTextAnswer;
	
	private Combo mComboAddressForm;
	private Combo mComboSecterQuestion;

	private DateTime mDateBirthday;
	
	private UserRepository mUserRepository;
	
	public RegistrationWindow() {
		mUserRepository = new UserRepository();
	}

	/**
	 * Open the window.
	 * @wbp.parser.entryPoint
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		mShlRegistration.open();
		mShlRegistration.layout();
		while (!mShlRegistration.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private void register() {
		// Validate first name
		if (mTextFirstName.getText().length() < 1) {
			showError("First Name must be not empty!");
			return;
		}
		
		// Validate last name
		if (mTextLastName.getText().length() < 1) {
			showError("Last Name must be not empty!");
			return;
		}
		
		// Validate email
		if (!mTextEmail.getText().matches(EMAIL_PATTERN)) {
			showError("Email is not correct!");
			return;
		}
		
		// Validate login
		if (!mTextLogin.getText().matches(LOGIN_PATTERN)) {
			showError("Login must not contains spaces!");
			return;
		}
		
		// Validate password
		if (!mTextPassword.getText().matches(PASSWORD_PATTERN)) {
			showError("Password must be more than 8 symbols, contains letters and numbers");
			return;
		}
		
		// Validate password again
		if (!mTextPasswordAgain.getText().matches(PASSWORD_PATTERN)) {
			showError("Password again must be more than 8 symbols, contains letters and numbers");
			return;
		}
		
		// Validate passwords
		if (!mTextPassword.getText().equals(mTextPasswordAgain.getText())) {
			showError("Passwords must be equals");
			return;
		}
		
		// Validate answer
		if (mTextAnswer.getText().length() < 1) {
			showError("Answer must be not empty!");
			return;
		}
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, mDateBirthday.getDay());
		calendar.set(Calendar.MONTH, mDateBirthday.getMonth());
		calendar.set(Calendar.YEAR, mDateBirthday.getYear());
		
		String firstName = mTextFirstName.getText();
		String lastName = mTextLastName.getText();
		String addressForm = mComboAddressForm.getText();
		Date birthday = calendar.getTime();
		
		String email = mTextEmail.getText();
		String login = mTextLogin.getText();
		String password = mTextPassword.getText();
		
		String secretQuestion = mComboSecterQuestion.getItems()[mComboSecterQuestion.getSelectionIndex()];
		String secretAnswer = mTextAnswer.getText();
		
		String photo = "";
		
		// Create new user
		User user = new User(login, 
				password, 
				firstName, 
				lastName, 
				birthday, 
				addressForm, 
				secretQuestion, 
				secretAnswer, 
				email, 
				photo);
		
		// Check if user already exists
		User foundUser = mUserRepository.getByLogin(login);
		
		if (foundUser != null) {
			showError("This user already exists");
			return;
		}
		
		// Add new user to store
		mUserRepository.add(user);
		
		showMessage("User successfully added! Please log in.");
		
		// Close the window
		mShlRegistration.dispose();
	}
	
	private void showMessage(String message) {
		MessageBox mb = new MessageBox(mShlRegistration);
		mb.setMessage(message);
		mb.open();
	}
	
	private void showError(String message) {
		MessageBox mb = new MessageBox(mShlRegistration);
		mb.setMessage(message);
		mb.open();
	}
	
	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		mShlRegistration = new Shell();
		mShlRegistration.setSize(321, 390);
		mShlRegistration.setText("Registration");
		
		Label labelFirstName = new Label(mShlRegistration, SWT.NONE);
		labelFirstName.setBounds(10, 10, 55, 15);
		labelFirstName.setText("First name");
		
		mTextFirstName = new Text(mShlRegistration, SWT.BORDER);
		mTextFirstName.setBounds(10, 31, 127, 21);
		
		Label labelLastName = new Label(mShlRegistration, SWT.NONE);
		labelLastName.setBounds(10, 58, 55, 15);
		labelLastName.setText("Last name");
		
		mTextLastName = new Text(mShlRegistration, SWT.BORDER);
		mTextLastName.setBounds(10, 79, 127, 21);
		
		Label labelBirthday = new Label(mShlRegistration, SWT.NONE);
		labelBirthday.setBounds(10, 153, 55, 15);
		labelBirthday.setText("Birthday");
		
		mDateBirthday = new DateTime(mShlRegistration, SWT.BORDER);
		mDateBirthday.setBounds(10, 174, 127, 24);
		
		Label labelAddressForm = new Label(mShlRegistration, SWT.NONE);
		labelAddressForm.setBounds(10, 106, 76, 15);
		labelAddressForm.setText("Address form");
		
		Label labelEmail = new Label(mShlRegistration, SWT.NONE);
		labelEmail.setBounds(165, 10, 55, 15);
		labelEmail.setText("Email");
		
		mTextEmail = new Text(mShlRegistration, SWT.BORDER);
		mTextEmail.setBounds(165, 31, 127, 21);
		
		Label labelLogin = new Label(mShlRegistration, SWT.NONE);
		labelLogin.setBounds(165, 58, 55, 15);
		labelLogin.setText("Login");
		
		mTextLogin = new Text(mShlRegistration, SWT.BORDER);
		mTextLogin.setBounds(165, 79, 127, 21);
		
		Label labelPassword = new Label(mShlRegistration, SWT.NONE);
		labelPassword.setBounds(165, 106, 55, 15);
		labelPassword.setText("Password");
		
		mTextPassword = new Text(mShlRegistration, SWT.BORDER);
		mTextPassword.setBounds(165, 126, 127, 21);
		mTextPassword.setEchoChar('*');
		
		Label labelPasswordAgain = new Label(mShlRegistration, SWT.NONE);
		labelPasswordAgain.setBounds(165, 153, 106, 15);
		labelPasswordAgain.setText("Password again");
		
		mTextPasswordAgain = new Text(mShlRegistration, SWT.BORDER);
		mTextPasswordAgain.setBounds(165, 177, 127, 21);
		mTextPasswordAgain.setEchoChar('*');
		
		mTextAnswer = new Text(mShlRegistration, SWT.BORDER);
		mTextAnswer.setBounds(10, 283, 282, 21);
		
		Label labelAnswer = new Label(mShlRegistration, SWT.NONE);
		labelAnswer.setBounds(10, 262, 55, 15);
		labelAnswer.setText("Answer");
		
		Button buttonRegister = new Button(mShlRegistration, SWT.NONE);
		buttonRegister.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				register();
			}
		});
		buttonRegister.setBounds(93, 317, 127, 25);
		buttonRegister.setText("Register");
		
		mComboSecterQuestion = new Combo(mShlRegistration, SWT.NONE);
		mComboSecterQuestion.setItems(new String[] {"What is name of your pet?", "What is last name of your first teacher?"});
		mComboSecterQuestion.setBounds(10, 233, 282, 23);
		mComboSecterQuestion.select(0);
		
		Label labelSecretQuestion = new Label(mShlRegistration, SWT.NONE);
		labelSecretQuestion.setBounds(10, 212, 127, 15);
		labelSecretQuestion.setText("Select secret question");
		
		mComboAddressForm = new Combo(mShlRegistration, SWT.NONE);
		mComboAddressForm.setItems(new String[] {"Mr.", "Mrs."});
		mComboAddressForm.setBounds(10, 127, 127, 23);
		mComboAddressForm.select(0);
	}
}
