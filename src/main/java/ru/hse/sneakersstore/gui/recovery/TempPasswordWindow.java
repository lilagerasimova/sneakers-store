package ru.hse.sneakersstore.gui.recovery;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ru.hse.sneakersstore.models.TempPassword;
import ru.hse.sneakersstore.models.User;
import ru.hse.sneakersstore.repository.TempPasswordRepository;
import ru.hse.sneakersstore.repository.UserRepository;

public class TempPasswordWindow {

	protected Shell mShell;

	private Text mTextLogin;
	private Text mTextTempPassword;

	private UserRepository mUserRepository;

	private TempPasswordRepository mPasswordRepository;

	public TempPasswordWindow() {
		mUserRepository = new UserRepository();
		mPasswordRepository = new TempPasswordRepository();
	}

	/**
	 * Open the window.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		mShell.open();
		mShell.layout();
		while (!mShell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private void runResetPasswordWindow(String login) {
		ResetPasswordWindow window = new ResetPasswordWindow(login);
		window.open();
	}
	
	
	private void submitTempPassword() {
		if (mTextLogin.getText().length() < 1) {
			showError("Login must not be empty");
			return;
		}
		
		if (mTextTempPassword.getText().length() < 1) {
			showError("Temp password must not be empty");
			return;
		}
		
		String login = mTextLogin.getText();
		String password = mTextTempPassword.getText();

		User user = mUserRepository.getByLogin(login);

		if (user == null) {
			showError("User is not found!");
			return;
		}

		List<TempPassword> foundPasswords = mPasswordRepository.getAllByUserId(user.getId());

		for (TempPassword item : foundPasswords) {
			if (item.getPassword().equals(password)) {
				runResetPasswordWindow(user.getLogin());
				return;
			}
		}
		
		showError("Granted password is not found!");
	}

	private void showMessage(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}

	private void showError(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		mShell = new Shell();
		mShell.setSize(156, 193);
		mShell.setText("Temp password");

		mTextLogin = new Text(mShell, SWT.BORDER);
		mTextLogin.setBounds(10, 31, 110, 21);

		Label labelLogin = new Label(mShell, SWT.NONE);
		labelLogin.setBounds(10, 10, 55, 15);
		labelLogin.setText("Login");

		Label labelTempPassword = new Label(mShell, SWT.NONE);
		labelTempPassword.setBounds(10, 58, 110, 15);
		labelTempPassword.setText("Temp Password");

		mTextTempPassword = new Text(mShell, SWT.BORDER);
		mTextTempPassword.setBounds(10, 79, 110, 21);
		mTextTempPassword.setEchoChar('*');

		Button buttonSubmit = new Button(mShell, SWT.NONE);
		buttonSubmit.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				submitTempPassword();
			}
		});
		buttonSubmit.setBounds(10, 116, 110, 25);
		buttonSubmit.setText("Submit");

	}

}
