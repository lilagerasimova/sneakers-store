package ru.hse.sneakersstore.gui.recovery;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ru.hse.sneakersstore.models.User;
import ru.hse.sneakersstore.repository.UserRepository;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class ResetPasswordWindow {

	private static final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,}$";
	
	protected Shell mShell;
	
	private Text mTextNewPassword;
	private Text mTextNewPasswordAgain;
	
	private UserRepository mUserRepository;
		
	private String mLogin;
	
	public ResetPasswordWindow(String login) {
		mUserRepository = new UserRepository();
		mLogin = login;
	}

	/**
	 * Open the window.
	 * @wbp.parser.entryPoint
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		mShell.open();
		mShell.layout();		
		while (!mShell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private void resetPassword() {
		// Validate new password
		if (!mTextNewPassword.getText().matches(PASSWORD_PATTERN)) {
			showError("Password must be more than 8 symbols, contains letters and numbers");
			return;
		}
		
		// Validate new password again
		if (!mTextNewPasswordAgain.getText().matches(PASSWORD_PATTERN)) {
			showError("Password again must be more than 8 symbols, contains letters and numbers");
			return;
		}
		
		String password = mTextNewPassword.getText();
		
		// Find current user
		User user = mUserRepository.getByLogin(mLogin);
		
		// Check that old password is not equals new password
		if (user.getPassword().equals(password)) {
			showError("New password shouldn't match with old");
			return;
		}
		
		// Set new password
		user.setPassword(password);
		// Disable auth flag
		user.setAuthBlocked(false);
		
		// Update current user
		mUserRepository.update(user);
		
		// Show message
		showMessage("Password for this user was reseted successfully!");
	}
	
	private void showMessage(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}
	
	private void showError(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}
	
	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		mShell = new Shell();
		mShell.setSize(184, 194);
		mShell.setText("Reset password");
		
		mTextNewPassword = new Text(mShell, SWT.BORDER);
		mTextNewPassword.setBounds(10, 31, 146, 21);
		mTextNewPassword.setEchoChar('*');
		
		Label labelPassword = new Label(mShell, SWT.NONE);
		labelPassword.setBounds(10, 10, 112, 15);
		labelPassword.setText("New password");
		
		Label labelNewPasswordAgain = new Label(mShell, SWT.NONE);
		labelNewPasswordAgain.setBounds(10, 58, 128, 15);
		labelNewPasswordAgain.setText("New password again");
		
		mTextNewPasswordAgain = new Text(mShell, SWT.BORDER);
		mTextNewPasswordAgain.setBounds(10, 79, 146, 21);
		mTextNewPasswordAgain.setEchoChar('*');
		
		Button buttonResetPassword = new Button(mShell, SWT.NONE);
		buttonResetPassword.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				resetPassword();
			}
		});
		buttonResetPassword.setBounds(10, 114, 146, 25);
		buttonResetPassword.setText("Reset password");

	}
}
