package ru.hse.sneakersstore.gui.recovery;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ru.hse.sneakersstore.models.User;
import ru.hse.sneakersstore.repository.UserRepository;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;

public class RecoveryWindow {

	protected Shell mShell;

	private Label mLabelQuestion;

	private Text mTextAnswer;
	private Text mTextLogin;

	private Button mButtonCheckAnswer;

	private UserRepository mUserRepository;

	private User mUser;
	private Link linkSendRequest;

	public RecoveryWindow() {
		mUserRepository = new UserRepository();
	}

	/**
	 * Open the window.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		mShell.open();
		mShell.layout();
		while (!mShell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private void runResetPasswordWindow() {
		ResetPasswordWindow window = new ResetPasswordWindow(mUser.getLogin());
		window.open();
	}

	private void runTempPasswordWindow() {
		TempPasswordWindow window = new TempPasswordWindow();
		window.open();
	}

	private void runSendRecoveryRequestWindow() {
		SendRecoveryRequestWindow window = new SendRecoveryRequestWindow();
		window.open();
	}

	private void findUser() {
		String login = mTextLogin.getText();

		mUser = mUserRepository.getByLogin(login);

		if (mUser == null) {
			showError("User is not found");
			return;
		}

		if (mUser.isAuthBlocked() || mUser.isBlocked() || mUser.isDeleted()) {
			showError("Action is not allowed for this user!");
			return;
		}

		mLabelQuestion.setText(mUser.getSecretQuestion());
		mButtonCheckAnswer.setEnabled(true);
	}

	private void checkAnswer() {
		if (mTextAnswer.getText().length() < 1) {
			showError("Asnwser on secret question must be not empty!");
			return;
		}
		
		String answer = mTextAnswer.getText();
		
		if (mUser.getSecretAnswer().equals(answer)) {
			runResetPasswordWindow();
			return;
		}

		// Mark user as blocked
		mUser.setAuthBlocked(true);
		// Block authorization for current user
		mUserRepository.update(mUser);
		// Show error
		showError("Wrong answer! Authorization for this user was blocked! Try to send request to andmin");
	}

	private void showMessage(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}

	private void showError(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		mShell = new Shell();
		mShell.setSize(213, 284);
		mShell.setText("Recovery variants");

		Label labelQuestionTitle = new Label(mShell, SWT.NONE);
		labelQuestionTitle.setBounds(10, 86, 169, 15);
		labelQuestionTitle.setText("Please answer on this question");

		mTextAnswer = new Text(mShell, SWT.BORDER);
		mTextAnswer.setBounds(10, 139, 169, 21);

		mButtonCheckAnswer = new Button(mShell, SWT.NONE);
		mButtonCheckAnswer.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				checkAnswer();
			}
		});
		mButtonCheckAnswer.setBounds(10, 166, 169, 25);
		mButtonCheckAnswer.setText("Check answer");
		mButtonCheckAnswer.setEnabled(false);

		Link linkTempPassword = new Link(mShell, SWT.NONE);
		linkTempPassword.setBounds(10, 197, 126, 15);
		linkTempPassword.setText("<a>Or use temp password</a>");
		linkTempPassword.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				runTempPasswordWindow();
			}
		});

		mTextLogin = new Text(mShell, SWT.BORDER);
		mTextLogin.setBounds(10, 28, 169, 21);

		Label labelLogin = new Label(mShell, SWT.NONE);
		labelLogin.setBounds(10, 10, 40, 15);
		labelLogin.setText("Login");

		Button buttonGetSectetQuestion = new Button(mShell, SWT.NONE);
		buttonGetSectetQuestion.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				findUser();
			}
		});
		buttonGetSectetQuestion.setBounds(10, 55, 169, 25);
		buttonGetSectetQuestion.setText("Get secret question");

		mLabelQuestion = new Label(mShell, SWT.NONE);
		mLabelQuestion.setBounds(10, 107, 169, 26);

		linkSendRequest = new Link(mShell, SWT.NONE);
		linkSendRequest.setBounds(10, 218, 143, 15);
		linkSendRequest.setText("<a>Or send request to admin</a>");
		linkSendRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				runSendRecoveryRequestWindow();
			}
		});
	}
}
