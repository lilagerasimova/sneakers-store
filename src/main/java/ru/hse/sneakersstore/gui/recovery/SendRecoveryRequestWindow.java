package ru.hse.sneakersstore.gui.recovery;

import java.util.Calendar;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ru.hse.sneakersstore.models.RecoveryRequest;
import ru.hse.sneakersstore.models.User;
import ru.hse.sneakersstore.repository.RecoveryRequestRepository;
import ru.hse.sneakersstore.repository.UserRepository;

public class SendRecoveryRequestWindow {

	protected Shell mShell;

	private Text mTextLogin;
	private Text mTextFirstName;
	private Text mTextLastName;
	private Text mTextEmail;

	private DateTime mDateBirthday;

	private Button buttonSendRequest;

	private UserRepository mUserRepository;

	private RecoveryRequestRepository mRequestRepository;

	public SendRecoveryRequestWindow() {
		mUserRepository = new UserRepository();
		mRequestRepository = new RecoveryRequestRepository();
	}

	/**
	 * Open the window.
	 * 
	 * @wbp.parser.entryPoint
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		mShell.open();
		mShell.layout();
		while (!mShell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private void sendRequest() {
		String login = mTextLogin.getText();

		User user = mUserRepository.getByLogin(login);

		if (user == null) {
			showMessage("User is not found!");
			return;
		}

		String firstName = mTextFirstName.getText();
		String lastName = mTextLastName.getText();
		String email = mTextEmail.getText();
		String birthday = String.format("%d/%d/%d", mDateBirthday.getDay(), mDateBirthday.getMonth(),
				mDateBirthday.getYear());

		String text = String.format("First name: %s, Last name: %s, email: %s, birthday: %s", firstName, lastName,
				email, birthday);

		RecoveryRequest request = new RecoveryRequest(user.getId(), user.getLogin(), text, false);
		
		mRequestRepository.add(request);
		
		showMessage("Request successfully sent to admin!");
	}

	private void showMessage(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}

	private void showError(String message) {
		MessageBox mb = new MessageBox(mShell);
		mb.setMessage(message);
		mb.open();
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		mShell = new Shell();
		mShell.setSize(160, 337);
		mShell.setText("Send request");

		Label labelFirstName = new Label(mShell, SWT.NONE);
		labelFirstName.setBounds(10, 58, 76, 15);
		labelFirstName.setText("First Name");

		mTextFirstName = new Text(mShell, SWT.BORDER);
		mTextFirstName.setBounds(10, 79, 126, 21);

		Label labelLastName = new Label(mShell, SWT.NONE);
		labelLastName.setBounds(10, 106, 76, 15);
		labelLastName.setText("Last Name");

		mTextLastName = new Text(mShell, SWT.BORDER);
		mTextLastName.setBounds(10, 127, 126, 21);

		Label labelBirthday = new Label(mShell, SWT.NONE);
		labelBirthday.setBounds(10, 154, 55, 15);
		labelBirthday.setText("Birthday");

		mDateBirthday = new DateTime(mShell, SWT.BORDER);
		mDateBirthday.setBounds(10, 175, 126, 24);

		Label labelEmail = new Label(mShell, SWT.NONE);
		labelEmail.setBounds(10, 205, 55, 15);
		labelEmail.setText("Email");

		mTextEmail = new Text(mShell, SWT.BORDER);
		mTextEmail.setBounds(10, 226, 126, 21);

		Label labelLogin = new Label(mShell, SWT.NONE);
		labelLogin.setBounds(10, 10, 55, 15);
		labelLogin.setText("Login");

		mTextLogin = new Text(mShell, SWT.BORDER);
		mTextLogin.setBounds(10, 31, 126, 21);

		buttonSendRequest = new Button(mShell, SWT.NONE);
		buttonSendRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				sendRequest();
			}
		});
		buttonSendRequest.setBounds(10, 266, 126, 25);
		buttonSendRequest.setText("Send request");
	}

}
