package ru.hse.sneakersstore.gui.main;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.SWT;
import org.eclipse.wb.swt.SWTResourceManager;

import ru.hse.sneakersstore.gui.profile.ProfileWindow;

import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;

public class MainWindow {

	protected Shell mShell;

	/**
	 * Open the window.
	 * @wbp.parser.entryPoint
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		mShell.open();
		mShell.layout();
		while (!mShell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	private void runProfileWindow() {
		ProfileWindow window = new ProfileWindow();
		window.open();
	}
	
	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		mShell = new Shell();
		mShell.setSize(450, 300);
		mShell.setText("Sneakers Store");
		
		Label labelTitle = new Label(mShell, SWT.NONE);
		labelTitle.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.NORMAL));
		labelTitle.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		labelTitle.setBounds(10, 10, 131, 32);
		labelTitle.setText("Main menu");
		
		Button buttonProfile = new Button(mShell, SWT.NONE);
		buttonProfile.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				runProfileWindow();
			}
		});
		buttonProfile.setBounds(10, 48, 75, 25);
		buttonProfile.setText("Profile");

	}

}
