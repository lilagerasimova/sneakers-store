package ru.hse.sneakersstore;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import ru.hse.sneakersstore.common.Const;
import ru.hse.sneakersstore.gui.login.LogInWindow;
import ru.hse.sneakersstore.models.User;
import ru.hse.sneakersstore.repository.UserRepository;

public class Main {

	public static User sAppUser = null;

	public static void main(String[] args) throws IOException {
		initFolders();
		initFiles();

		LogInWindow window = new LogInWindow();
		window.open();
	}

	private static void initFolders() {
		try {
			if (!Files.exists(Paths.get(Const.FOLDERS.FILES)))
				Files.createDirectory(Paths.get(Const.FOLDERS.FILES));
			if (!Files.exists(Paths.get(Const.FOLDERS.SYS)))
				Files.createDirectory(Paths.get(Const.FOLDERS.SYS));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void initFiles() {
		try {
			if (!Files.exists(Paths.get(Const.FILES.USERS))) {
				Files.createFile(Paths.get(Const.FILES.USERS));
				initAdminUser();
			}
			if (!Files.exists(Paths.get(Const.FILES.RECOVERY_REUESTS)))
				Files.createFile(Paths.get(Const.FILES.RECOVERY_REUESTS));
			if (!Files.exists(Paths.get(Const.FILES.TEMP_PASSWORDS)))
				Files.createFile(Paths.get(Const.FILES.TEMP_PASSWORDS));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void initAdminUser() {
		User user = new User("admin", 
				"Admin1234", 
				"F", 
				"L", 
				new Date(), 
				"Mr.", 
				"What is name of your pet?", 
				"Murzik", 
				"admin@admin.com", 
				"");
		
		user.setVerified(true);
		user.setAdmin(true);
		
		UserRepository repository = new UserRepository();
		repository.add(user);
	}
}
