package ru.hse.sneakersstore.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.hse.sneakersstore.store.StoreModel;

public class RecoveryRequest extends StoreModel {
	
	@Expose
	@SerializedName("user_id")
	private long mUserId;
	@Expose
	@SerializedName("login")
	private String mLogin;
	@Expose
	@SerializedName("text")
	private String mText;
	@Expose
	@SerializedName("is_resolved")
	private boolean mIsResolved;
	
	public RecoveryRequest(long userId, String login, String text, boolean isResolved) {
		super(0);
		mUserId = userId;
		mLogin = login;
		mText = text;
		mIsResolved = isResolved;
	}

	public long getUserId() {
		return mUserId;
	}

	public void setUserId(long userId) {
		this.mUserId = userId;
	}

	public String getLogin() {
		return mLogin;
	}

	public void setLogin(String login) {
		this.mLogin = login;
	}

	public String getText() {
		return mText;
	}

	public void setText(String text) {
		this.mText = text;
	}

	public boolean isResolved() {
		return mIsResolved;
	}

	public void setResolved(boolean isResolved) {
		this.mIsResolved = isResolved;
	}
}
