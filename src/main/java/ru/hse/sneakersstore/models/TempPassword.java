package ru.hse.sneakersstore.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.hse.sneakersstore.store.StoreModel;

public class TempPassword extends StoreModel {

    @Expose
    @SerializedName("user_id")
	private long mUserId;
    @Expose
    @SerializedName("password")
	private String mPassword;
	
	public TempPassword(long userId, String password) {
		super(0);
		mUserId = userId;
		mPassword = password;
	}

	public long getUserId() {
		return mUserId;
	}

	public void setUserId(long userId) {
		this.mUserId = userId;
	}

	public String getPassword() {
		return mPassword;
	}

	public void setPassword(String password) {
		this.mPassword = password;
	}
}
