package ru.hse.sneakersstore.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import ru.hse.sneakersstore.store.StoreModel;

import java.util.Date;

public class User extends StoreModel {

    @Expose
    @SerializedName("login")
    private String mLogin;
    @Expose
    @SerializedName("password")
    private String mPassword;
    @Expose
    @SerializedName("first_name")
    private String mFirstName;
    @Expose
    @SerializedName("last_name")
    private String mLastName;
    @Expose
    @SerializedName("birthday")
    private Date mBirthday;
    @Expose
    @SerializedName("address_form")
    private String mAddressForm;
    @Expose
    @SerializedName("secret_question")
    private String mSecretQuestion;
    @Expose
    @SerializedName("secret_answer")
    private String mSecretAnswer;
    @Expose
    @SerializedName("email")
    private String mEmail;
    @Expose
    @SerializedName("photo")
    private String mPhoto;
    @Expose
    @SerializedName("is_admin")
    private boolean mIsAdmin;
    @Expose
    @SerializedName("is_analyst")
    private boolean mIsAnalyst;
    @Expose
    @SerializedName("is_auth_blocked")
    private boolean mIsAuthBlocked;
    @Expose
    @SerializedName("is_blocked")
    private boolean mIsBlocked;
    @Expose
    @SerializedName("is_verified")
    private boolean mIsVerified;
    @Expose
    @SerializedName("is_deleted")
    private boolean mIsDeleted;

    public User(String login,
                String password,
                String firstName,
                String lastName,
                Date birthday,
                String addressForm,
                String secretQuestion,
                String secretAnswer,
                String email,
                String photo) {

        super(0);
        mLogin = login;
        mPassword = password;
        mFirstName = firstName;
        mLastName = lastName;
        mBirthday = birthday;
        mAddressForm = addressForm;
        mSecretQuestion = secretQuestion;
        mSecretAnswer = secretAnswer;
        mEmail = email;
        mPhoto = photo;
    }

    public User(long id,
                String login,
                String password,
                String firstName,
                String lastName,
                Date birthday,
                String addressForm,
                String secretQuestion,
                String secretAnswer,
                String email,
                String photo,
                boolean isAdmin,
                boolean isAnalyst,
                boolean isAuthBlocked,
                boolean isBlocked,
                boolean isVerified,
                boolean isActive) {

        super(id);
        mLogin = login;
        mPassword = password;
        mFirstName = firstName;
        mLastName = lastName;
        mBirthday = birthday;
        mAddressForm = addressForm;
        mSecretQuestion = secretQuestion;
        mSecretAnswer = secretAnswer;
        mEmail = email;
        mPhoto = photo;
        mIsAdmin = isAdmin;
        mIsAnalyst = isAnalyst;
        mIsAuthBlocked = isAuthBlocked;
        mIsBlocked = isBlocked;
        mIsVerified = isVerified;
        mIsDeleted = isActive;
    }

    public String getLogin() {
        return mLogin;
    }

    public void setLogin(String login) {
        mLogin = login;
    }

    public String getPassword() {
        return mPassword;
    }

    public void setPassword(String password) {
        mPassword = password;
    }

    public String getFirstName() {
        return mFirstName;
    }

    public void setFirstName(String firstName) {
        mFirstName = firstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public void setLastName(String lastName) {
        mLastName = lastName;
    }

    public Date getBirthday() {
        return mBirthday;
    }

    public void setBirthday(Date birthday) {
        mBirthday = birthday;
    }

    public String getAddressForm() {
        return mAddressForm;
    }

    public void setAddressForm(String addressForm) {
        mAddressForm = addressForm;
    }

    public String getSecretQuestion() {
        return mSecretQuestion;
    }

    public void setSecretQuestion(String secretQuestion) {
        mSecretQuestion = secretQuestion;
    }

    public String getSecretAnswer() {
        return mSecretAnswer;
    }

    public void setSecretAnswer(String secretAnswer) {
        mSecretAnswer = secretAnswer;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public String getPhoto() {
        return mPhoto;
    }

    public void setPhoto(String photo) {
        mPhoto = photo;
    }

    public boolean isAdmin() {
        return mIsAdmin;
    }

    public void setAdmin(boolean admin) {
        mIsAdmin = admin;
    }

    public boolean isAnalyst() {
        return mIsAnalyst;
    }

    public void setAnalyst(boolean analyst) {
        mIsAnalyst = analyst;
    }

    public boolean isAuthBlocked() {
		return mIsAuthBlocked;
	}

	public void setAuthBlocked(boolean isAuthBlocked) {
		this.mIsAuthBlocked = isAuthBlocked;
	}

	public boolean isBlocked() {
        return mIsBlocked;
    }

    public void setBlocked(boolean blocked) {
        mIsBlocked = blocked;
    }

    public boolean isVerified() {
        return mIsVerified;
    }

    public void setVerified(boolean verified) {
        mIsVerified = verified;
    }

    public boolean isDeleted() {
        return mIsDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        mIsDeleted = isDeleted;
    }
}
