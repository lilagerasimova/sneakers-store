package ru.hse.sneakersstore.common;

public final class Const {

	public static final class FOLDERS {
		public static final String FILES = "files";
		public static final String SYS = FILES + "/sys";
	}

	public static final class FILES {
		public static final String USERS = FOLDERS.SYS + "/users.sys";
		public static final String RECOVERY_REUESTS = FOLDERS.SYS + "/recovery_requests.sys";
		public static final String TEMP_PASSWORDS = FOLDERS.SYS + "/temp_passwords.sys";
	}
}
